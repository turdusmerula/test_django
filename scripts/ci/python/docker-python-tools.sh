#!/bin/bash
set -x

# python tools are running inside a docker
SONARSCANNER_CONTAINER=sonarqube_sonar-scanner_1

# deactivate to debug
DOCKER_OPTS="--rm"

[ -z "$cwd" ] && cwd=$(pwd)
[ -z "$PYTHON_IMAGE" ] && export PYTHON_IMAGE="testdjango_django_1"
[ -z "$CONTAINER_ID" ] && export CONTAINER_ID=$(mktemp -d XXXXXXX)
#[ -z "$PYTHON_CONTAINER" ] && export PYTHON_CONTAINER=${PYTHON_IMAGE}_${CONTAINER_ID}
[ -z "$PYTHON_CONTAINER" ] && export PYTHON_CONTAINER=${PYTHON_IMAGE}


function create_container() {
	docker-compose -f dev.yml up --build -d	
}

function drop_container() {
	# remove container
#	docker-compose  -f dev.yml stop
#	docker-compose  -f dev.yml rm -f
	echo
}

function coverage() {
	command=$*
	
	sudo docker exec $PYTHON_CONTAINER sh -c "cd $(pwd) ; PYTHONPATH=$PYTHONPATH coverage $command"
	return $?
}

function python() {
	command=$*

	sudo docker exec $PYTHON_CONTAINER sh -c "cd $(pwd) ; PYTHONPATH=$PYTHONPATH python $command"
	return $?
}

function pytest() {
	command=$*
	
	sudo docker exec $PYTHON_CONTAINER sh -c "DATABASE_URL=$DATABASE_URL pytest $command"
	return $?
}

function sonar-scanner() {
	command=$*

	sudo docker exec $SONARSCANNER_CONTAINER sh -c "cd $(pwd) ; PYTHONPATH=$PYTHONPATH sonar-scanner $command"
	return $?
}

#function docker-compose() {
#	command=$*
#
#	docker exec $PYTHON_CONTAINER sh -c "cd $(pwd) ; docker-compose $command"
#	return $?
#}