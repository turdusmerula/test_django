#!/bin/bash
function get_ip() {
	ifconfig "$1" | grep -e "inet ad*r" | sed -e "s#.*inet ad*r:\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\).*#\1#g" | head -n1
	
	return 0	
}

function get_ips() {
	ifconfig | grep -e "inet ad*r" | sed -e "s#.*inet ad*r:\([0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\.[0-9][0-9]*\).*#\1#g"
	
	return 0	
}

function show_exposed_port() {
	port=$1
	
	sep=""
	for ip in $(get_ips)
	do
		echo -n "${sep}${ip}:${port}"
		sep=" - "
	done
	
	return 0
}

function get_default_gateway() {
	route -n | sed "s#[[:space:]][[:space:]]*# #g" | grep -e "^0.0.0.0" | cut -d ' ' -f2
}

function get_default_netmask() {
	route -n | sed "s#[[:space:]][[:space:]]*# #g" | grep -e "^0.0.0.0" | cut -d ' ' -f3
}

function exist_docker_network() {
	docker network inspect "$1" 2>/dev/null >/dev/null
	return $?
}

function remove_docker_network() {
	network=$1
	
	exist_docker_network "$network" && {
		sudo docker network rm "$network"
		return 0
	}
	return 1
}
