#!/bin/bash
function get_image() {
	name=$1
	image=$(docker images | sed "s#[[:space:]][[:space:]]*# #g" | grep -e "^${name} " | cut -d' ' -f3)
	[ -z "${image}" ] && return 1
	echo -n "${image}"	
	return 0
}

function remove_docker_image() {
	name=$1
	image=$(get_image "$name")
	if [ -n "$image" ]
	then
		docker rmi "$image"
		return $?
	fi
	return 0
}

function get_container() {
	name=$1
	container=$(docker ps | sed "s#[[:space:]][[:space:]]*# #g" | grep -e " ${name}$" | cut -d' ' -f1)
	[ -z "${container}" ] && return 1
	echo -n "${container}"	
	return 0
}

function get_all_container() {
	name=$1
	container=$(docker ps -a | sed "s#[[:space:]][[:space:]]*# #g" | grep -e " ${name}$" | cut -d' ' -f1)
	[ -z "${container}" ] && return 1
	echo -n "${container}"	
	return 0
}

function remove_container() {
	name=$1
	container=$(get_container "$name")
	
	if [ -n "$container" ]
	then
		docker stop "$name" || return 1
		echo "Remove $container container"
		docker rm "$container"
		return 0
	fi

	container=$(get_all_container "$name")
	if [ -n "$container" ]
	then
		echo "Remove $container container"
		docker rm "$container"
		return 0
	fi
	
	return 1
}

function get_image_ip() {
	name=$1

	container=$(get_container "$name")
	if [ -z "$container" ]
	then
		echo "Error: Container '$name' does not exist" >&2
		return 1
	fi

	ip="$(docker inspect "${container}" | grep '"IPAddress":' | sed 's/.*"\([0-9]*\.[0-9]*\.[0-9]*.[0-9]*\).*",/\1/g' | tail -n1)"
	
	echo "$ip"
	
	return 0
}
